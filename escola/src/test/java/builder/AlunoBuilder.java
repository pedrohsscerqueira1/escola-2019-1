package builder;
import br.ucsal.bes20191.testequalidade.escola.domain.Aluno;
import br.ucsal.bes20191.testequalidade.escola.domain.SituacaoAluno;

public class AlunoBuilder {
	Aluno aluno;
	
	public Aluno criarAluno(){
		aluno = new Aluno();
		aluno.setMatricula(1);
		aluno.setNome("Bruno");
		aluno.setSituacao(SituacaoAluno.ATIVO);
		aluno.setAnoNascimento(2009);
		return aluno;
	}
}

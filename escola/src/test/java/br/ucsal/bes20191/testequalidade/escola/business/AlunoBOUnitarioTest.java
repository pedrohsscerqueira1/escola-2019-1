package br.ucsal.bes20191.testequalidade.escola.business;

import br.ucsal.bes20191.testequalidade.escola.business.AlunoBO;
import br.ucsal.bes20191.testequalidade.escola.domain.Aluno;
import br.ucsal.bes20191.testequalidade.escola.persistence.AlunoDAO;
import br.ucsal.bes20191.testequalidade.escola.util.DateHelper;
import builder.AlunoBuilder;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;
import org.junit.Assert;

public class AlunoBOUnitarioTest {
	private AlunoBO alunoBO;
	private AlunoDAO alunoDAO;
	private DateHelper dateUtil;
	private AlunoBuilder alunoBuilder;
	
	@Before
	public void setup(){
		alunoDAO = Mockito.mock(AlunoDAO.class);
		dateUtil = new DateHelper();
		alunoBO =  new AlunoBO(alunoDAO, dateUtil);
		alunoBuilder = new AlunoBuilder();
		Mockito.when(alunoDAO.encontrarPorMatricula(1)).thenReturn(alunoBuilder.criarAluno());
		
		
		
	}
	/**
	 * Verificar o calculo da idade. Caso de teste: aluno nascido em 2003 ter�
	 * 16 anos.
	 */
	@Test
	public void testarCalculoIdadeAluno1() {
		Integer matricula = 1;
		Integer esperado = 10;
		Integer obtido = alunoBO.calcularIdade(matricula);
		Assert.assertEquals(esperado, obtido);
	}
	
	/**
	 * Verificar se alunos ativos s�o atualizados.
	 */
	@Test
	public void testarAtualizacaoAlunosAtivos() {
		Aluno aluno = alunoBuilder.criarAluno();
		alunoBO.atualizar(aluno);
		Mockito.verify(alunoDAO).salvar(aluno);
		
	}


}

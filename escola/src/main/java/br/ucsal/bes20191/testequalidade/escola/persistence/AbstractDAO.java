package br.ucsal.bes20191.testequalidade.escola.persistence;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class AbstractDAO {

	private static final String USER = "postgres";
	private static final String PASSWORD = "postgresql";
	private static final String STRING_CONNECTION = "jdbc:postgresql://localhost:6543/cst-testequalidade";

	private Connection connection = null;

	protected Connection getConnection() {
		if (connection == null) {
			conectar();
		}
		return connection;
	}

	private void conectar() {
		try {
			Class.forName("org.postgresql.Driver");
			connection = DriverManager.getConnection(STRING_CONNECTION, USER, PASSWORD);
		} catch (ClassNotFoundException | SQLException e) {
			e.printStackTrace();
		}
	}

}
